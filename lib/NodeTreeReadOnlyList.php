<?php

/**
 * Simple type for one level depth only lists that provides a better display
 * that array or object implementations.
 */
class Plumber_NodeTreeReadOnlyList extends Plumber_NodeTreeAbstract
{
  /**
   * @var array|object
   */
  protected $_data;

  protected function _loadChildren() {
    foreach ($this->_data as $key => $value) {
      if (is_scalar($value)) {
        $this->_children[$value] = new Plumber_NodeValueReadOnlyScalar($key, $value);
      }
      else {
        // FIXME: This doesn't fit here.
      }
    }
  }

  public function hasChildren() {
    return !empty($this->_data);
  }

  public function countChildren() {
    return count($this->_data);
  }

  /**
   * Constructor.
   * 
   * @param string $name
   *   Human readable name.
   * @param array|object $value
   *   Array value.
   * @param bool $ajax = TRUE
   *   (optional) Set to FALSE to disable AJAX loading.
   */
  public function __construct($name, $value = NULL, $ajax = TRUE) {
    $this->_data = $value;
    parent::__construct($name, $ajax);
  }
}
