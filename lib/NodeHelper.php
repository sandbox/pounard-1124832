<?php

class Plumber_NodeHelper
{
  /**
   * Find a matching instance for given value. This will be used in last
   * resort when no module gave an implementation for the given value.
   * 
   * @param mixed &$value
   * 
   * @return Plumber_NodeValueInterface
   *   NULL if none found.
   */
  public static function getDefaultInstanceForValue($name, &$value, $ajaxEnabled = FALSE) {
    if (is_scalar($value)) {
      return new Plumber_NodeValueReadOnlyScalar($name, $value);
    }
    else if (is_array($value)) {
      return new Plumber_NodeTreeReadOnlyArray($name, $value, $ajaxEnabled, FALSE);
    }
    else if (is_object($value)) {
      return new Plumber_NodeTreeReadOnlyObject($name, $value, $ajaxEnabled, FALSE);
    }
    else {
      // FIXME: Unknown stuff.
      return NULL;
    }
  }

  /**
   * Introspect modules and find the right instance for given value.
   * 
   * @param mixed $value
   * 
   * @return Plumber_NodeValueInterface
   *   NULL if none found.
   */
  public static function getInstanceForValue($name, &$value, $ajaxEnabled = FALSE) {
    $hook = 'plumberize';
    foreach (module_implements($hook) as $module) {
      if ($node = module_invoke($module, $hook, $name, $value, $ajaxEnabled)) {
        return $node;
      }
    }
    return self::getDefaultInstanceForValue($name, $value, $ajaxEnabled);
  }
}
