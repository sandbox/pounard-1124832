<?php

class Plumber_Node_Count extends Plumber_NodeTreeAbstract
{
  protected function _loadChildren() {
    $this->_children['total'] = new Plumber_NodeValueReadOnlyRaw('total', $this->countChildren());

    $result = db_query("SELECT t.type, COUNT(n.type) AS count FROM {node_type} t LEFT JOIN {node} n ON n.type = t.type GROUP BY n.type ORDER BY t.type ASC");
    while ($data = db_fetch_object($result)) {
      $this->_children['total_' . $data->type] = new Plumber_NodeValueReadOnlyRaw($data->type, $data->count);
    }
  }

  public function hasChildren() {
    return TRUE;
  }

  protected $_count;

  public function countChildren() {
    if (!isset($this->_count)) {
      $this->_count = db_result(db_query("SELECT COUNT(nid) FROM {node}"));
    }
    return $this->_count;
  }

  public function __construct() {
    parent::__construct('count', TRUE);
  }
}
