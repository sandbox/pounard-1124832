<?php

/**
 * Valued item that can be displayed and edited into the tree view.
 */
interface Plumber_NodeValueEditableInterface extends Plumber_NodeValueInterface
{
  /**
   * Return Drupal FAPI compatible editable element for this particular node
   * edition.
   * 
   * @return array
   */
  public function getFormElement();

  /**
   * Validate new value.
   * 
   * Use form_set_error() when your element doesn't validate.
   * 
   * @param mixed &$value
   *   Value from the FAPI element. Can be an array if the element is a complex
   *   FAPI widget.
   *   You can modify this value while validating for later use in the save()
   *   method.
   */
  public function validate(&$value);

  /**
   * Save new value.
   * 
   * This method must reset the current object value to the new one being saved
   * because later getHumanReadable() calls will be done to give the new result
   * back through the AJAX response.
   * 
   * @param mixed $value
   *   Value from the FAPI element. Can be an array if the element is a complex
   *   FAPI widget.
   */
  public function save($value);
}
