<?php

class Plumber_System_SystemTableList extends Plumber_NodeTreeAbstract
{
  /**
   * @var bool
   */
  protected $_active;

  /**
   * @var string
   */
  protected $_type;

  /**
   * Early count when children are not being accounted for.
   * 
   * @var int
   */
  protected $_count;

  protected function _loadChildren() {
    switch ($this->_type) {
      case 'module':
        $class = 'Plumber_System_ModuleEntry';
        break;
      case 'theme':
        $class = 'Plumber_System_ThemeEntry';
        break;
      default:
        $class = 'Plumber_System_SystemTableEntry';
    }
    $result = db_query("SELECT * FROM {system} WHERE status = %d AND type = '%s' ORDER BY weight ASC, name ASC", array(
      $this->_active ? 1 : 0,
      $this->_type,
    ));
    while ($info = db_fetch_object($result)) {
      $info->info = unserialize($info->info);
      $this->_children[$info->name] = new $class($info->name, $info);
    }
  }

  protected function _count() {
    if (!isset($this->_count)) {
      if (isset($this->_children)) {
        $this->_count = count($this->_children);
      }
      else {
        $this->_count = db_result(db_query("SELECT COUNT(name) FROM {system} WHERE status = %d AND type = '%s' ORDER BY weight ASC, name ASC", array(
          $this->_active ? 1 : 0,
          $this->_type,
        )));
      }
    }
  }

  public function hasChildren() {
    $this->_count();
    return $this->_count > 0;
  }

  public function countChildren() {
    return $this->_count;
  }

  /**
   * Override the default constructor, that awaits for a name.
   */
  public function __construct($type, $active = TRUE, $namePrefix = '') {
    $this->_type = $type;
    $this->_active = $active;
    $this->_data = 
    parent::__construct($namePrefix . ($active ? "enabled" : "disabled"), TRUE);
  }
}
