<?php

class Plumber_System_Hook extends Plumber_NodeTreeAbstract
{
  /**
   * @var string
   */
  protected $_hook;

  /**
   * @var array
   */
  protected $_modules;

  protected function _loadChildren() {
    foreach ($this->_modules as $module) {
      $this->_children[$module] = new Plumber_NodeString($module);
    }
  }

  public function hasChildren() {
    return 0 < count($this->_modules);
  }

  public function countChildren() {
    return count($this->_modules);
  }

  /**
   * Override the default constructor, that awaits for a name.
   */
  public function __construct($hook) {
    $this->_hook = $hook;
    $this->_modules = module_implements($hook);
    parent::__construct($hook, TRUE);
  }
}
