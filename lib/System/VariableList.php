<?php

class Plumber_System_VariableList extends Plumber_NodeTreeAbstract
{
  protected function _loadChildren() {
    // Attempt a smart variable split using module names.
    $modules = module_list();

    // Modules are being checked in reverse order (more specialized first) to
    // ensure that 'views_admin_*' variables goes to to 'views_admin' module
    // and not 'views' module.
    krsort($modules);

    // Copy the array, we will depopulate it while checking the module names.
    // The remaining array, at the end, will be our orphan variables.
    $variables = $GLOBALS['conf'];

    foreach ($modules as $module) {
      $names = array();

      foreach ($variables as $name => $value) {
        if (0 === strpos($name, $module)) {
          $names[] = $name;
          unset($variables[$name]);
        }
      }

      if (!empty($names)) {
        $this->_children[$module] = new Plumber_System_VariableSubList($module, $names);
      }
    }

    // Resort currently found modules.
    ksort($this->_children);

    // If not empty, then we have orphan, there are chances we always have
    // orphans here.
    if (!empty($variables)) {
      $this->_children['orphans'] = new Plumber_System_VariableSubList('orphans', array_keys($variables));
    }
  }

  public function hasChildren() {
    return 0 < $this->countChildren();
  }

  /**
   * @var int
   */
  protected $_count;

  public function countChildren() {
    if (!isset($this->_count)) {
      $this->_count = count($GLOBALS['conf']);
    }
    return $this->_count;
  }

  /**
   * Override the default constructor, that awaits for a name.
   */
  public function __construct() {
    parent::__construct("variables", TRUE);
  }
}
