<?php

class Plumber_System_HookList extends Plumber_NodeTreeAbstract
{
  protected function _getHookNames() {
    return array(
      'nodeapi',
      'menu',
      'theme',
      'footer',
      'init',
      'exit',
      'form_alter',
    );
  }

  protected function _loadChildren() {
      // Attempt a smart variable split using module names.
    $modules = module_list();

    // Modules are being checked in reverse order (more specialized first) to
    // ensure that 'views_admin_*' variables goes to to 'views_admin' module
    // and not 'views' module.
    krsort($modules);

    $hooks = $this->_getHookNames();
    sort($hooks);

    foreach ($modules as $module) {
      $names = array();

      foreach ($hooks as $key => $name) {
        if (0 === strpos($name, $module)) {
          $names[] = $name;
          unset($hooks[$key]);
        }
      }

      if (!empty($names)) {
        $children = array();
        foreach ($names as $hook) {
          $children[$hook] = new Plumber_System_Hook($hook);
        }
        $this->_children[$module] = new Plumber_NodeTreeDefault($module, $children, TRUE);
      }
    }

    // Resort currently found modules.
    ksort($this->_children);

    // If not empty, then we have orphan, there are chances we always have
    // orphans here.
    if (!empty($hooks)) {
      $children = array();
      foreach ($hooks as $hook) {
        $children[$hook] = new Plumber_System_Hook($hook);
      }
      $this->_children['orphans'] = new Plumber_NodeTreeDefault('orphans', $children, TRUE);
    }
  }

  public function hasChildren() {
    return 0 < count($this->_getHookNames());
  }

  /**
   * @var int
   */
  protected $_count;

  public function countChildren() {
    return count($this->_getHookNames());
  }

  /**
   * Override the default constructor, that awaits for a name.
   */
  public function __construct() {
    parent::__construct('hooks', TRUE);
  }
}
