<?php

class Plumber_System_VariableScalar
  extends Plumber_NodeValueReadOnlyScalar
  implements Plumber_NodeValueEditableInterface
{
  public function getFormElement() {
    switch ($this->_type) {

      case Plumber_NodeValueReadOnlyScalar::INT:
      case Plumber_NodeValueReadOnlyScalar::FLOAT:
      case Plumber_NodeValueReadOnlyScalar::STRING:
        return array(
          '#type' => 'textfield',
          '#size' => 20,
          '#default_value' => $this->_value,
        );

      case Plumber_NodeValueReadOnlyScalar::BOOL:
        return array(
          '#type' => 'checkbox',
          '#default_value' => $this->_value,
        );

      case Plumber_NodeValueReadOnlyScalar::UNKNOWN:
        return array();
    }
  }

  public function validate(&$value) {
    switch ($this->_type) {

      case Plumber_NodeValueReadOnlyScalar::INT:
        if (!preg_match('/^(-|)\d+$/', trim($value))) {
          form_set_error(t("Value must be an valid integer."));
        }
        break;

      case Plumber_NodeValueReadOnlyScalar::FLOAT:
        if (!preg_match('/^(-|)\d+(\.\d+|)$/', trim($value))) {
          form_set_error(t("Value must be an valid float."));
        }
        break;

      case Plumber_NodeValueReadOnlyScalar::STRING:
      case Plumber_NodeValueReadOnlyScalar::BOOL:
      case Plumber_NodeValueReadOnlyScalar::UNKNOWN:
        break;
    }
  }

  public function save($value) {
    $this->_value = $value;
    variable_set($this->_name, $value);
  }

  public function __construct($name, $value = NULL) {
    parent::__construct($name, $value);
  }
} 