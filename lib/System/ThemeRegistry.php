<?php

class Plumber_System_ThemeRegistry extends Plumber_NodeString implements Plumber_NodeTreeInterface
{
  /**
   * @var array
   */
  protected $_registry;

  protected $_children;

  protected function _initChildren() {
    if (!isset($this->_children)) {
      foreach ($this->_registry as $hook => $info) {
        $this->_children[$hook] = new Plumber_NodeTreeReadOnlyArray($hook, $info);
      }
    }
  }

  public function getChildren() {
    $this->_initChildren();
    return $this->_children;
  }

  public function getChild($identifier) {
    $this->_initChildren();
    if (isset($this->_children[$identifier])) {
      return $this->_children[$identifier];
    }
    throw new Exception("Child '" . $identifier . "' not found.");
  }

  public function hasChildren() {
    return !empty($this->_registry);
  }

  public function countChildren() {
    return count($this->_registry);
  }

  public function getName() {
    return "registry";
  }

  public function isAjax() {
    return TRUE;
  }

  protected function _loadRegistry($theme) {
    // Check the theme registry cache; if it exists, use it.
    $cache = cache_get('theme_registry:' . $theme->name, 'cache');
    if (isset($cache->data)) {
      $this->_registry = $cache->data;
    }
    else {
      // If not, build one and cache it.
      $engine = isset($theme->info['engine']) ? $theme->info['engine'] : NULL;
      $base_theme = isset($theme->info['base theme']) ? $theme->info['base theme'] : NULL;
      $this->_registry = _theme_build_registry($theme, $base_theme, $theme_engine);
      _theme_save_registry($this->_theme, $this->_registry);
    }
  }

  /**
   * Override the default constructor, that awaits for a name.
   */
  public function __construct($theme_key, $theme) {
    $this->_loadRegistry($theme);
  }
}
