<?php

class Plumber_System_SystemTableEntry
  extends Plumber_NodeTreeAbstract
  implements Plumber_NodeValueEditableInterface
{
  /**
   * @var string
   */
  protected $_name;

  /**
   * @var array
   */
  protected $_info;

  protected function _loadChildren() {
    // Misc information.
    foreach (array('name', 'project', 'description', 'package') as $key) {
      $this->_children[$key] = new Plumber_NodeValueReadOnlyScalar($key, $this->_info->info[$key]);
    }
    foreach (array('filename', 'throttle', 'bootstrap', 'schema_version') as $key) {
      $this->_children[$key] = new Plumber_NodeValueReadOnlyScalar($key, $this->_info->$key);
    }
    $this->_children['info'] = new Plumber_NodeTreeReadOnlyArray('raw info', $this->_info->info, FALSE, FALSE);
  }

  public function hasChildren() {
    return TRUE;
  }

  public function countChildren() {
    return Plumber_NodeTreeInterface::COUNT_UNDETERMINED;
  }

  public function getRawValue() {
    return $this->_info->status;
  }

  public function isPreviewTruncated() {
    return FALSE;
  }

  public function getHumanReadableValue($preview = TRUE) {
    return $this->_info->status ? t("enabled") : t("disabled");
  }

  public function getFormElement() {
    return array(
      '#type' => 'checkbox',
      '#title' => t("enabled"),
      '#default_value' => $this->_info->status,
      '#enabled' => $this->_info->type === 'module',
    );
  }

  public function validate(&$value) {}

  public function save($value) {
    if ($value) {
      module_enable(array($this->_info->name));
      drupal_set_message(t("Module <em>@module</em> has been <strong>enabled</strong>.", array('@module' => $this->_info->name)));
    }
    else {
      module_disable(array($this->_info->name));
      drupal_set_message(t("Module <em>@module</em> has been <strong>disabled</strong>.", array('@module' => $this->_info->name)));
    }
    $this->_info->status = $value;
  }

  public function __construct($name, $info) {
    $this->_name = $name;
    $this->_info = $info;
    parent::__construct($name, TRUE);
  }
}
