<?php

class Plumber_System_ThemeEntry extends Plumber_System_SystemTableEntry
{
  protected function _loadChildren() {
    // Misc information.
    foreach (array('name', 'description', 'base_theme') as $key) {
      $this->_children[$key] = new Plumber_NodeValueReadOnlyScalar($key, $this->_info->info[$key]);
    }
    foreach (array('filename') as $key) {
      $this->_children[$key] = new Plumber_NodeValueReadOnlyScalar($key, $this->_info->$key);
    }
    $this->_children['info'] = new Plumber_NodeTreeReadOnlyArray('raw info', $this->_info->info, FALSE, FALSE);

    
    // For themes, theme registry is a good candidate for display.
    if ($this->_info->status) {
      $this->_children['registry'] = new Plumber_System_ThemeRegistry($this->_info->name, $this->_info);
    }
  }
}
