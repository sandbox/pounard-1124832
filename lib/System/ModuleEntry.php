<?php

class Plumber_System_ModuleEntry extends Plumber_System_SystemTableEntry
{
  protected function _loadChildren() {
    // Misc information.
    foreach (array('name', 'project', 'description', 'package') as $key) {
      $this->_children[$key] = new Plumber_NodeValueReadOnlyScalar($key, $this->_info->info[$key]);
    }
    foreach (array('filename', 'throttle', 'bootstrap', 'schema_version') as $key) {
      $this->_children[$key] = new Plumber_NodeValueReadOnlyScalar($key, $this->_info->$key);
    }
    // Dependencies and dependents.
    foreach (array('dependencies', 'dependents') as $key) {
      if (isset($this->_info->info[$key]) && !empty($this->_info->info[$key])) { 
        $this->_children[$key] = new Plumber_NodeTreeReadOnlyList($key, $this->_info->info[$key], FALSE);
      }
    }
    $this->_children['info'] = new Plumber_NodeTreeReadOnlyArray('raw info', $this->_info->info, FALSE, FALSE);
  }
}
