<?php

class Plumber_System_VariableSubList extends Plumber_NodeTreeAbstract
{
  /**
   * @var array
   */
  protected $_names;

  protected function _loadChildren() {
    foreach ($this->_names as $name) {
      $value =& $GLOBALS['conf'][$name];

      // Find out which variable type is.
      if (is_scalar($value)) {
        $this->_children[$name] = new Plumber_System_VariableScalar($name, $value);
      }
      else if ($node = Plumber_NodeHelper::getInstanceForValue($name, $value, TRUE)) {
        $this->_children[$name] = $node;
      }
    }
  }

  public function hasChildren() {
    return 0 < count($this->_names);
  }

  /**
   * @var int
   */
  protected $_count;

  public function countChildren() {
    return count($this->_names);
  }

  /**
   * Override the default constructor, that awaits for a name.
   */
  public function __construct($name, $names) {
    $this->_names = $names;
    parent::__construct($name, TRUE);
  }
}
