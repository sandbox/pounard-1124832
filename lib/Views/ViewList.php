<?php

class Plumber_Views_ViewList extends Plumber_NodeString implements Plumber_NodeTreeInterface
{
  protected $_views;

  protected $_children;

  public function getChildren() {
    if (!isset($this->_children)) {
      $this->_children = array();
      foreach ($this->_views as $view_name => $view) {
        $this->_children[$view_name] = new Plumber_Views_View($view);
      }
    }
    return $this->_children;
  }

  public function getChild($identifier) {
    if (isset($this->_children[$identifier])) {
      return $this->_children[$identifier];
    }
    else if (isset($this->_views[$identifier])) {
      return new Plumber_Views_View($this->_views[$identifier]);
    }
    throw new Exception("Child '" . $identifier . "' not found.");
  }

  public function hasChildren() {
    return !empty($this->_views);
  }

  public function countChildren() {
    return count($this->_views);
  }

  public function getName() {
    return "views";
  }

  public function isAjax() {
    return FALSE;
  }

  /**
   * Default constructor.
   */
  public function __construct() {
    $this->_views = views_get_all_views();
  }
}
