<?php

class Plumber_Views_View extends Plumber_NodeTreeAbstract
{
  /**
   * @var view
   */
  protected $_view;

  protected function _loadChildren() {
    foreach ($this->_view->display as $display_id => $display) {
      $this->_children['display-' . $display_id] = new Plumber_NodeTreeReadOnlyObject("Display: " . $display_id, $display, FALSE, FALSE);
    }
  }

  public function hasChildren() {
    return TRUE;
  }

  public function countChildren() {
    return Plumber_NodeTreeInterface::COUNT_UNDETERMINED;
  }

  /**
   * Default constructor.
   * 
   * @param view $view
   */
  public function __construct(view $view) {
    $this->_view = $view;
    parent::__construct($this->_view->name, TRUE);
  }
}