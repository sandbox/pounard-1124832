<?php

class Plumber_Content_Type extends Plumber_NodeTreeAbstract
{
  protected function _loadChildren() {
    $this->_children['fields'] = new Plumber_Content_FieldList($this->_name);
  }

  public function __construct($nodeTypeName) {
    parent::__construct($nodeTypeName, FALSE);
  }
}
