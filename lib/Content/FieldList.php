<?php

class Plumber_Content_FieldList extends Plumber_NodeTreeAbstract
{
  /**
   * @var string
   */
  protected $_nodeTypeName;

  protected function _loadChildren() {
    $fieldInfo = _content_type_info();
    foreach ($fieldInfo['content types'][$this->_nodeTypeName] as $fieldName => $fieldInfo) {
      $this->_children[$fieldName] = new Plumber_NodeTreeReadOnlyArray($fieldName, $fieldInfo, FALSE, FALSE);
    }
  }

  protected $_count;

  public function hasChildren() {
    return 0 < $this->_count;
  }

  public function countChildren() {
    return $this->_count;
  }

  public function __construct($nodeTypeName) {
    parent::__construct('fields', TRUE);
    $this->_nodeTypeName = $nodeTypeName;
    $fieldInfo = _content_type_info();
    $this->_count = count($fieldInfo['content types'][$this->_nodeTypeName]);
  }
}
