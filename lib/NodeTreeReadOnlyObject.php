<?php

/**
 * PHP object value node.
 */
class Plumber_NodeTreeReadOnlyObject extends Plumber_NodeTreeReadOnlyArray
{
  public function getType() {
    return "object";
  }

  public function getHash() {
    return $this->_hash;
  }

  /**
   * @var bool
   */
  protected $_notEmpty;

  public function hasChildren() {
    return $this->_notEmpty;
  }

  /**
   * Constructor.
   * 
   * @param string $name
   *   Human readable name.
   * @param object $value
   *   Array value.
   * @param bool $ajax = TRUE
   *   (optional) Set to FALSE to disable AJAX loading.
   */
  public function __construct($name, $value = NULL, $ajax = TRUE, $ajaxChildren = FALSE) {
    $this->_ajaxChildren = $ajaxChildren;
    $this->_data = $value;
    $this->_name = $name;
    $this->_ajax = $ajax;
    $this->_count = Plumber_NodeTreeInterface::COUNT_UNDETERMINED;
    $this->_notEmpty = !empty($value);
    $this->_hash = spl_object_hash($value);
  }
}
