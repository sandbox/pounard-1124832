<?php

/**
 * Simple node that aims replacing already rendered nodes.
 */
class Plumber_NodeRecursion extends Plumber_NodeString
{
  public function __construct($name) {
    parent::__construct($name);
  }
}
