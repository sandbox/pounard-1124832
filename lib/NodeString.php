<?php

/**
 * Simple string name node.
 */
class Plumber_NodeString implements Plumber_NodeInterface
{
  /**
   * @var string
   */
  protected $_name;

  public function __construct($name) {
    $this->_name = $name;
  }

  public function getName() {
    return $this->_name;
  }

  public function getType() {
    return NULL;
  }

  public function getStatus() {
    return NULL;
  }

  protected $_hash;

  /**
   * Default implementation uses this current object hash. For more complex
   * business object, this seems a really bad idea.
   * 
   * @see Plumber_NodeInterface::getHash()
   */
  public function getHash() {
    if (!isset($this->_hash)) {
      $this->_hash = spl_object_hash($this);
    }
    return $this->_hash;
  }
}
