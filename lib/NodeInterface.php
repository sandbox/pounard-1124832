<?php

/**
 * Item that can be displayed into the tree view.
 */
interface Plumber_NodeInterface
{
  /**
   * Get displayable name.
   * 
   * @return string
   */
  public function getName();

  /**
   * Get human readable type.
   * 
   * @return string
   */
  public function getType();

  /**
   * Get current node status.
   * 
   * Some specific implementation might want to display a business specific
   * human readable status label.
   * 
   * Use it wisely, if non useful, return NULL here, too much status messages
   * would make the tree overloaded with useless information.
   * 
   * @return string
   */
  public function getStatus();

  /**
   * Set context.
   * 
   * @param Plumber_TreeContext $context
   */
  //public function setContext(Plumber_TreeContext $context);

  /**
   * Get context.
   * 
   * @return Plumber_TreeContext
   */
  //public function getContext();

  /**
   * Compute and get a hash corresponding to business data, and not object
   * identity.
   */
  public function getHash();
}
