<?php

/**
 * PHP array value node.
 */
class Plumber_NodeTreeReadOnlyArray extends Plumber_NodeTreeAbstract
{
  const HASH_KEY = '__hash__';

  /**
   * @var bool
   */
  protected $_ajaxChildren = TRUE;

  /**
   * @var array|object
   */
  protected $_data;

  protected function _loadChildren() {
    foreach ($this->_data as $key => &$value) {
      if ($key == self::HASH_KEY) {
        continue;
      }
      if ($node = Plumber_NodeHelper::getDefaultInstanceForValue($key, $value, $this->_ajaxChildren)) {
        $this->_children[$key] = $node;
      }
    }
  }

  /**
   * @var int
   */
  protected $_count;

  public function hasChildren() {
    // At least two elements are required, hash makes one.
    return 0 < $this->_count;
  }

  public function countChildren() {
    return $this->_count;
  }

  public function getType() {
    return "array";
  }

  public function getHash() {
    if (!isset($this->_hash)) {
      if (!isset($this->_data[self::HASH_KEY])) {
        $this->_hash = $this->_data[self::HASH_KEY] = uniqid(NULL, TRUE);
      }
      else {
        $this->_hash = $this->_data[self::HASH_KEY];
      }
    }
    return $this->_data[self::HASH_KEY];
  }

  /**
   * Constructor.
   * 
   * @param string $name
   *   Human readable name.
   * @param array $value
   *   Array value.
   * @param bool $ajax = TRUE
   *   (optional) Set to FALSE to disable AJAX loading.
   */
  public function __construct($name, array &$value = NULL, $ajax = TRUE, $ajaxChildren = FALSE) {
    $this->_ajaxChildren = $ajaxChildren;
    $this->_data =& $value;
    $this->_name = $name;
    $this->_ajax = $ajax;
    $this->_count = count($this->_data);
    $this->_hash = $this->getHash();
  }
}
