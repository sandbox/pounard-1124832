<?php

/**
 * Valued item that can be displayed into the tree view.
 */
interface Plumber_NodeValueInterface extends Plumber_NodeInterface
{
  /**
   * Get raw value.
   * 
   * @return mixed
   */
  public function getRawValue();

  /**
   * Tell if the current human readable value preview is truncated in order
   * to display seamlessly into the UI.
   * 
   * @return bool
   */
  public function isPreviewTruncated();

  /**
   * Render human readable string of the value.
   * 
   * @param bool $preview = TRUE
   *   If set to TRUE, this method must return an output that is shorted enough
   *   that it won't break the rendered UI. If you do so, you must return TRUE
   *   into the isPreviewTruncated() in order for the UI adapt to be able to
   *   spawn a dialog that will display the full value.
   * 
   * @return array|string
   *   drupal_render() compatible data, or a raw HTML string.
   */
  public function getHumanReadableValue($preview = TRUE);
}
