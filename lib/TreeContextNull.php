<?php

/**
 * Implementation that do not check recursion.
 */
class Plumber_TreeContextNull extends Plumber_TreeContext
{
  public function hasDone(Plumber_NodeInterface $node) {
    return FALSE;
  }
}
