<?php

/**
 * Base class for misc. implementations that need children lazzy loading.
 * 
 * Instanciate your children into the _loadChildren() method which will be
 * called only if necessary if AJAX mode is set. This method must fill up
 * the _children array property with Plumber_NodeInteface instances.
 * 
 * If your object is meant to be loaded using AJAX, you'll need to override
 * the hasChildren() and countChildren() method to ensure they won't init
 * the children.
 * 
 * If you override the constructor, always call the parent constructor to
 * ensure this object will be fully initialized.
 */
abstract class Plumber_NodeTreeAbstract extends Plumber_NodeString implements Plumber_NodeTreeInterface
{
  /**
   * @var array
   */
  protected $_children;

  /**
   * Load all children.
   * 
   * You must implement this in order to use this base implementation.
   */
  protected abstract function _loadChildren();

  /**
   * Initiliaze children.
   */
  protected function _initChildren() {
    if (!isset($this->_children)) {
      $this->_loadChildren();
    }
  }

  public function getChildren() {
    $this->_initChildren();
    return $this->_children;
  }

  public function getChild($identifier) {
    $this->_initChildren();
    if (isset($this->_children[$identifier])) {
      return $this->_children[$identifier];
    }
    throw new Exception("Child '" . $identifier . "' not found.");
  }

  public function hasChildren() {
    $this->_initChildren();
    return !empty($this->_children);
  }

  public function countChildren() {
    $this->_initChildren();
    return count($this->_children);
  }

  /**
   * @var bool
   */
  protected $_ajax;

  public function isAjax() {
    return $this->_ajax;
  }

  /**
   * Default constructor. If you override it, please call the parent
   * implementation after you inited your own values.
   * 
   * @param string $name
   *   Displayed name.
   * @param bool $ajax
   */
  public function __construct($name, $ajax = TRUE) {
    parent::__construct($name);
    $this->_ajax = $ajax;
  }
}