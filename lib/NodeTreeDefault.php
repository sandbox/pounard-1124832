<?php

/**
 * Item that can be displayed into the tree view, that has children.
 */
class Plumber_NodeTreeDefault extends Plumber_NodeString implements Plumber_NodeTreeInterface
{
  /**
   * @var array
   */
  protected $_children;

  /**
   * Add a child.
   * 
   * @param string $identifier
   * @param Plumber_NodeInterface $node
   */
  public function addChild($identifier, Plumber_NodeInterface $node) {
    $this->_children[$identifier] = $node;
  }

  public function getChildren() {
    return $this->_children;
  }

  public function getChild($identifier) {
    if (isset($this->_children[$identifier])) {
      return $this->_children[$identifier];
    }
    throw new Exception("Children '" . $identifier . "' not found.");
  }

  public function hasChildren() {
    return !empty($this->_children);
  }

  /**
   * @var bool
   */
  protected $_displayCount;

  public function countChildren() {
    return $this->_displayCount ? count($this->_children) : Plumber_NodeTreeInterface::COUNT_UNDETERMINED;
  }

  /**
   * @var bool
   */
  protected $_ajax = TRUE;

  public function isAjax() {
    return $this->_ajax;
  }

  /**
   * Constructor.
   * 
   * @param string $name
   *   Human readable name.
   * @param array $children = array()
   *   (optional) Already filled array of Plumber_NodeInterface elements.
   * @param bool $ajax = TRUE
   *   (optional) Set to FALSE to disable AJAX loading.
   */
  public function __construct($name, array $children = array(), $ajax = TRUE, $displayCount = TRUE) {
    parent::__construct($name);
    $this->_ajax = $ajax;
    $this->_displayCount = $displayCount;
    $this->_children = $children;
  }
}
