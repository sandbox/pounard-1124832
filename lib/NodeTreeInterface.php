<?php

/**
 * Item that can be displayed into the tree view, that has children.
 * 
 * Hierarchical elements have a unique path in order to be able to fetch
 * single elements. Tree nodes have the responsability of setting the
 * path to their children, using the one given by their own parents.
 */
interface Plumber_NodeTreeInterface extends Plumber_NodeInterface
{
  const COUNT_UNDETERMINED = -1;

  /**
   * Get all children.
   * 
   * @return array
   *   Key/value pairs, keys are value names values are Plumber_NodeInterface
   *   living instances.
   */
  public function getChildren();

  /**
   * Get a particular child. This will be used in AJAX callbacks for
   * loading.
   * 
   * @param string $identifier
   * 
   * @return Plumber_NodeInterface
   * 
   * @throws Exception
   *   If child does not exist.
   */
  public function getChild($identifier);

  /**
   * Tell if the current node has children.
   * 
   * This method must not load children nodes, for performances.
   * 
   * @return bool
   */
  public function hasChildren();

  /**
   * Count children.
   * 
   * This method must not load children nodes, for performances, except if it
   * cannot do it otherwise.
   * 
   * @return int
   *   This method can return Plumber_NodeTreeInterface::COUNT_UNDETERMINED
   *   if it can help providing better performances. It will only alter the
   *   final UI.
   */
  public function countChildren();

  /**
   * Tree nodes that have a lot of children or a deep hierarchy should always
   * goes to TRUE here, to avoid the page to be too long to init.
   * 
   * @return bool
   */
  public function isAjax();
}
