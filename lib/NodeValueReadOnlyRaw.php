<?php

/**
 * Raw value to display.
 */
class Plumber_NodeValueReadOnlyRaw extends Plumber_NodeString implements Plumber_NodeValueInterface
{
  /**
   * @var mixed
   */
  protected $_value;

  /**
   * Constructor.
   * 
   * @param string $name
   *   Name.
   * @param mixed $value
   *   (optional) Value.
   * @param int $type = self::AUTO
   *   (optional) If set to any other type, will stick to this.
   */
  public function __construct($name, $value = NULL) {
    parent::__construct($name);
    $this->_value = $value;
  }

  public function getRawValue() {
    return $this->_value;
  }

  public function isPreviewTruncated() {
    return FALSE;
  }

  public function getHumanReadableValue($preview = TRUE) {
    return (string) $this->_value;
  }
}
