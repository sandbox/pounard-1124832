<?php

/**
 * PHP scalar value node.
 */
class Plumber_NodeValueReadOnlyScalar extends Plumber_NodeString implements Plumber_NodeValueInterface
{
  /**
   * Type is unknown.
   */
  const UNKNOWN = 999;

  /**
   * Type is integer.
   */
  const INT = 1;

  /**
   * Type is float.
   */
  const FLOAT = 2;

  /**
   * Type is string.
   */
  const STRING = 3;

  /**
   * Type is boolean.
   */
  const BOOL = 4;

  /**
   * Auto-detect type at construct time.
   */
  const AUTO = 0;

  /**
   * @var mixed
   */
  protected $_value;

  public function getRawValue() {
    return $this->_value;
  }

  public function getType() {
    switch ($this->_type) {
      case self::INT:
        return "integer";
      case self::FLOAT:
        return "float";
      case self::STRING:
        return "string";
      case self::BOOL:
        return "boolean";
      case self::UNKNOWN:
        return "unknown";
      default:
        throw new Exception("Unhandled type.");
    }
  }

  public function isPreviewTruncated() {
    return ($this->_type == self::STRING) && strlen($this->_value) > PLUMBER_STRING_SPLIT_SIZE; 
  }

  public function getHumanReadableValue($preview = TRUE) {
    if (!isset($this->_value)) {
      return '<em>null</em>';
    }
    else {
      switch ($this->_type) {

        case self::BOOL:
          return $this->_value ? 'true' : 'false';

        case self::STRING:
          $output = (string) $this->_value;
          $suffix = '';

          if (strlen($this->_value) > PLUMBER_STRING_SPLIT_SIZE) {
            $output = substr($output, 0, PLUMBER_STRING_SPLIT_SIZE);
            $suffix = '&nbsp;<em>...</em>';
          }

          return '<code>' . htmlentities($output, ENT_COMPAT, 'UTF-8') . '</code>' . $suffix;

        default:
          return (string) $this->_value;
      }
    }
  }

  /**
   * Constructor.
   * 
   * @param string $name
   *   Name.
   * @param mixed $value
   *   (optional) Value.
   * @param int $type = self::AUTO
   *   (optional) If set to any other type, will stick to this.
   */
  public function __construct($name, $value = NULL, $type = self::AUTO) {
    parent::__construct($name);

    if ($this->_type == self::AUTO) {
      if (is_bool($value)) {
        $type = self::BOOL;
      }
      else if (is_int($value)) {
        $type = self::INT;
      }
      else if (is_float($value)) {
        $type = self::FLOAT;
      }
      else if (is_string($value)) {
        $type = self::STRING;
      }
      else {
        $type = self::UNKNOWN;
      }
    }

    $this->_type = $type;
    // Force exceptions to raise at construct time if type is not handled.
    $this->getType();

    $this->_value = $value;
  }
}
