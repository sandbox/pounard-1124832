<?php

class Plumber_TreeContext
{
  /**
   * Recursion breaker.
   */
  protected $_done = array();

  /**
   * Tell if the current context already has displayed the object.
   * 
   * @param Plumber_NodeInterface $node
   *   Node to store into the recursion hashmap.
   * @param string $path
   *   Serialized path as a string for later link.
   * 
   * @return bool|string
   *   Serialized first occurence path, if found, FALSE otherwise.
   */
  public function hasDone(Plumber_NodeInterface $node, $path = TRUE) {
    $hash = $node->getHash();

    if (!isset($this->_done[$hash])) {
      $this->_done[$hash] = $path;
      return FALSE;
    }

    return $this->_done[$hash];
  }
}