<tr<?php print $row_attributes;?>>
  <td class="plumber-expand-inline">
    <a name="<?php print $id; ?>"></a>
    <?php print $indent; ?>
    <div class="plumber-expand">&nbsp;</div>
    <div class="plumber-name">
      <?php print $name; ?>
      <?php if (isset($recursion_id)): ?>
      <a href="#<?php print $recursion_id; ?>" rel="<?php print $recursion_id; ?>" class="plumber-recursion"><em>*recursion*</em></a>
      <?php endif; ?>
    </div>
  </td>
  <td class="plumber-column-status">
    <?php if (isset($status)): print $status; endif; ?>
  </td>
  <td class="plumber-column-type">
    <?php if (isset($type)): print $type; endif; ?>
  </td>
  <td class="plumber-column-value">
    <?php if (isset($value)): print $value; endif; ?>
  </td>
  <td class="plumber-column-op">
    <?php if ($refreshable):?>
    <div class="plumber-refresh"></div>
    <?php endif; ?>
    <?php if (isset($operations)): print $operations; endif; ?>
  </td>
</tr>
<?php foreach ($children as $rendered_children): ?>
<?php print $rendered_children; ?>
<?php endforeach; ?>