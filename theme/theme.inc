<?php

function _plumber_theme_init() {
  static $init = FALSE;

  if (!$init) {
    $module_path = drupal_get_path('module', 'plumber') . '/theme/';

    $settings = array(
      'ajax' => array(
        'tree' => url('admin/plumber/ajax/tree'),
        'edit' => url('admin/plumber/ajax/edit'),
      )
    );

    drupal_add_css($module_path . 'plumber.css');
    drupal_add_js($module_path . 'plumber.js');
    drupal_add_js('misc/jquery.form.js');

    if (module_exists('jquery_ui')) {
      $settings['jquery_ui'] = TRUE;
      jquery_ui_add(array('ui.core', 'ui.dialog'));
      // Also add default jQuery UI theme.
      drupal_add_css(drupal_get_path('module', 'jquery_ui') . '/jquery.ui/themes/default/ui.all.css');
    }

    drupal_add_js(array('plumber' => $settings), 'setting');
    $init = TRUE;
  } 
}

/**
 * Implementation of template_preprocess_TEMPLATE().
 */
function template_preprocess_plumber_tree(&$variables) {
  _plumber_theme_init();
  $context = isset($variables['context']) ? $variables['context'] : new Plumber_TreeContextNull;

  $path = $variables['path'];
  $variables['depth'] = count($path);
  $variables['rows'] = array();
  $attributes = array();

  foreach ($variables['nodes'] as $identifier => $node) {
    $variables['rows'][] = theme('plumber_row', $node, $context, $path, $identifier);
  }

  if (count($path) == 0) {
    $attributes['class'] = 'plumber-tree';
  }

  $variables['tree_attributes'] = drupal_attributes($attributes);
}

/**
 * Implementation of template_preprocess_TEMPLATE().
 */
function template_preprocess_plumber_row(&$variables) {
  $variables['children'] = array();
  $context = isset($variables['context']) ? $variables['context'] : new Plumber_TreeContextNull;

  // Current node being displayed in tree.
  $node = $variables['node'];

  // Javascript helper.
  $path = $variables['path'];
  $parent_path_string = implode(PLUMBER_PATH_SEP, $path);
  $path[] = $variables['identifier'];
  $variables['id'] = $path_string = implode(PLUMBER_PATH_SEP, $path);

  // Break recursion right here.
  if ($recursion_id = $context->hasDone($node, $path_string)) {
    if (is_string($recursion_id)) {
      $variables['recursion_id'] = $recursion_id;
    }
    $node = new Plumber_NodeRecursion($node->getName());
  }

  // New depth for children;
  $depth = count($path);
  $operations = array();

  // Javascript expandable control.
  $expandable = $expanded = FALSE;
  $attributes = array('class' => 'plumber-tree-node', 'id' => $path_string);

  // Indent display.
  $indent = '';
  for ($i = 1; $i < $depth; $i++) {
    $indent .= '<div class="plumber-indent">&nbsp;</div>';
  }
  $variables['indent'] = $indent;

  $variables['name'] = $node->getName();

  // Children, if any. Also compute the expandable state.
  if ($node instanceof Plumber_NodeTreeInterface && $node->hasChildren()) {
    if (Plumber_NodeTreeInterface::COUNT_UNDETERMINED !== ($count = $node->countChildren())) {
      $variables['name'] .= ' <small><em>' . $count . '</em></small>';
    }

    if ($node->isAjax()) {
      $attributes['class'] .= ' plumber-ajax';
    }
    else {
      $variables['children'][] = theme('plumber_tree', $node->getChildren(), $context, $path);
    }

    $expandable = TRUE;
  }

  // Value.
  if ($node instanceof Plumber_NodeValueInterface) {
    $variables['value'] = $node->getHumanReadableValue();
  }
  if ($node instanceof Plumber_NodeValueEditableInterface) {
    $attributes['class'] .= ' plumber-editable';
  }
  $variables['type'] = $node->getType();

  // Prepare HTML rendering, for Javascript usage.
  if ($depth > 1 && !$expanded) {
    $attributes['style'] = 'display: none;';
  }
  if ($expanded) {
    $attributes['class'] .= ' plumber-expanded';
  }
  if (!empty($parent_path_string)) {
    $attributes['rel'] = $parent_path_string;
  }
  if ($expandable) {
    $attributes['class'] .= ' plumber-expandable';
  }

  $variables['row_attributes'] = drupal_attributes($attributes);

  foreach ($operations as $key => $operation) {
    $operations[$key] = $operation;
  }
  $variables['operations'] = implode('', $operations);
  $variables['refreshable'] = TRUE;
}
