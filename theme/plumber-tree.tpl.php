<?php if ($depth == 0): ?>
<table<?php print $tree_attributes;?>>
  <?php if ($header): ?>
  <thead>
    <tr>
      <th>Name</th>
      <th>Status</th>
      <th>Type</th>
      <th>Value</th>
      <th></th>
    </tr>
  </thead>
  <?php endif; ?>
  <tbody>
<?php endif; ?>
    <?php foreach ($rows as $row): ?>
    <?php print $row; ?>
    <?php endforeach; ?>
<?php if ($depth == 0): ?>
  </tbody>
</table>
<?php endif; ?>
