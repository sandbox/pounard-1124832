/**
 * @file
 * Plumber Tree.
 * 
 * First and quite ugly implementation. Better to come.
 */

;(function( $ ) {

  Drupal.behaviors.PlumberTree = function( context ) {
    $( '.plumber-tree:not(.plumber-tree-processed)', context )
      .addClass( 'plumber-tree-processed' )
      .plumbertree();
  };

  $.extend({

    _plumbercommon: {

      /**
       * Check if AJAX response is valid.
       * 
       * @return bool
       *   false if errenous, true if valid. 
       */
      checkResponse: function( response ) {
        if ( response.messages ) {
          $._plumbercommon.displayMessages( response.messages );
        }
        return response.success;
      },

      /**
       * Display messages from AJAX response.
       */
      displayMessages: function ( messages ) {
        var output = $( '<div></div>' );

        if ( typeof messages == 'string' ) {
          output.html( messages );
        }
        else {
          // FIXME: Display messages.
        }

        if ( Drupal.settings.plumber.jquery_ui ) {

          output.css({'position': 'relative'});
          output.append( '<input style="position: absolute; bottom: 16px; right: 16px;" type="button" class="plumber-dialog-close" value="' + Drupal.t("Close") + '"/>' );
          output.find( 'input.plumber-dialog-close' ).click( function( event ) {
            event.stopPropagation();
            output.dialog( 'close' );
            return false;
          });

          $( document.body ).append( output );
          output.dialog({
            width: 400,
            modal: true,
            heigh: 'auto'
          });
        }
        else {
          // FIXME: Find something else.
        }
      }
    },

    _plumbernode: {

      /**
       * Definitely destroy the given node.
       */
      destroy: function( options, empty ) {
        options.element
          .removeClass( 'plumber-loading' )
          .removeClass( 'plumber-expandable' )
          .removeClass( 'plumber-expanded' );

        if ( empty ) {
          options.element.addClass( 'plumber-empty' );
        }

        $._plumbernode.disable( options );

        options.element._plumbernode = null;
      },

      /**
       * Temporary disable the given node.
       */
      disable: function( options ) {
        options.element.find( '.plumber-expand' ).unbind( 'click' );
        options.element.find( '.plumber-refresh' ).hide().unbind( 'click' );
      },

      /**
       * Enable or create a node.
       */
      enable: function( options ) {
        options.element.find( '.plumber-expand' ).unbind( 'click' ).click( function( event ) {
          event.stopPropagation();
          $._plumbernode.handlerLoadChildren( options );
          return false;
        });
        options.element.find( '.plumber-refresh' ).show().unbind( 'click' ).click( function( event ) {
          event.stopPropagation();
          $._plumbernode.handlerRefresh( options );
          return false;
        });
      },
  
      /**
       * Find node children within full tree.
       */
      findChildren: function( options ) {
        options.children = options.tree.find( '.plumber-tree-node[rel="' + options.path + '"]' );
      },
  
      /**
       * Run AJAX callback for given node, and populate its children.
       */
      populateAjax: function( options ) {
        options.element.addClass( 'plumber-loading' );

        // We won't disable the element here, because it already has been disabled
        // within the click trigger.
        $.ajax({
          'method': 'GET',
          'async': true,
          'url': Drupal.settings.plumber.ajax.tree,
          'data': { 'path': options.path },
          'dataType': 'json',
          'success': function( data, textStatus ) {
            if ( $._plumbercommon.checkResponse( data ) && data.html ) {
              options.element.after( data.html );
  
              // Any new node must be found (won't else because the original
              // behavior is looking for full tree).
              options.tree.plumbertree();
  
              // Proceed to click trigger where we stopped before.
              $._plumbernode.findChildren( options );
              $._plumbernode.handlerLoadChildren( options );

              options.element.removeClass( 'plumber-loading' );
            }
            else {
              $._plumbernode.destroy( options, true );
            }
          },
          'error': function() {
            $._plumbernode.destroy( options, true );
          }
        });
      },
  
      /**
       * Open current node.
       */
      expand: function( options ) {
        options.children.show();
        options.expanded = true;
        options.element.addClass( 'plumber-expanded' );
      },

      /**
       * Recursively destroy children.
       */
      destroyChildren: function( options ) {
        if ( options.children ) {
          options.children.each(function() {
            var childOptions = this._plumbernode ? this._plumbernode : {};

            if ( childOptions.expanded ) {
              $._plumbernode.destroyChildren( childOptions );
            }
          });

          // Destroy and remove from DOM.
          $._plumbernode.destroy( options );
          options.children.remove();
        }

        options.expanded = false;
        options.element.removeClass( 'plumber-expanded' );
      },

      /**
       * Recursively collapse node.
       */
      collapse: function( options ) {
        // Recursive children collapse.
        if ( options.children ) {
          options.children.each(function() {
            var childOptions = this._plumbernode ? this._plumbernode : {};

            if ( childOptions.expanded ) {
              $._plumbernode.collapse( childOptions );
            }
          });

          options.children.hide();
        }

        options.expanded = false;
        options.element.removeClass( 'plumber-expanded' );
      },

      /**
       * Refresh button click even callback.
       */
      handlerRefresh: function( options ) {
        // Disable events while doing stuff, to avoid user clicking more than
        // once and triggering the same operation twice.
        $._plumbernode.disable( options );

        // Destroy children, and force the container to be AJAX.
        $._plumbernode.destroyChildren( options );
        options.isAjax = true;
        options.children = null;

        // Trigger AJAX load.
        $._plumbernode.populateAjax( options );
      },

      /**
       * Click event callback.
       */
      handlerLoadChildren: function( options ) {
        var error = false;

        // Disable events while doing stuff, to avoid user clicking more than
        // once and triggering the same operation twice.
        $._plumbernode.disable( options );

        if ( ! options.children ) {
          // While element is marked for AJAX rendering, trigger the populate
          // callback.
          if ( options.isAjax ) {
            $._plumbernode.populateAjax( options );
            return;
          }

          // In case the element is not AJAX rendered, just find its children.
          $._plumbernode.findChildren( options );
        }

        // No children means this node can die.
        if ( ! options.children.length ) {
          $._plumbernode.destroy( options );
          return;
        }

        if ( options.expanded )
          $._plumbernode.collapse( options );
        else
          $._plumbernode.expand( options );

        if ( ! error ) {
          $._plumbernode.enable( options );
        }
        else {
          // In case of any error, this node is not good enough for later use,
          // definitely destroy it.
          $._plumbernode.destroy( options );
        }
      }
    },

    _plumbereditable: {

      /**
       * Definitely destroy the given node.
       */
      destroy: function( options ) {
        $._plumbereditable.disable( options );
      },

      /**
       * Temporary disable the given node.
       */
      disable: function( options ) {
        options.valueContainer.unbind( 'click' );
      },

      /**
       * Enable or create a node.
       */
      enable: function( options ) {
        options.valueContainer.unbind( 'click' ).click( function( event ) {
          event.stopPropagation();
          $._plumbereditable.handlerEdit( options );
          return false;
        });
      },

      /**
       * Get form response, attach form.
       */
      attachForm: function( options, response ) {
        var originalContent = options.valueContainer.html();

        var revertValueContainer = function() {
          options.valueContainer.html( originalContent );
          $._plumbereditable.enable( options );
        };

        // Attach new form.
        options.valueContainer.html( response.html );
        Drupal.attachBehaviors( options.valueContainer );

        var form = options.valueContainer.find( 'form' ),
            action = form.attr( 'action' );

        if ( form.length && action ) {
          form.ajaxForm( {
            'success': function( data, textStatus ) {
              if ( $._plumbercommon.checkResponse( data ) && data.value ) {
                 options.valueContainer.html( data.value );
                 $._plumbereditable.enable( options );
               }
               else {
                 revertContainer();
               }
             },
             'error': function() {
               revertValueContainer();
             },
            'dataType': 'json',
            'url': action,
          });

          // Cancel button behavior.
          form.find( '.plumber-button-cancel' ).click( function( event ) {
            event.stopPropagation();
            event.preventDefault();
            revertValueContainer();
            return false;
          });
        }
        else {
          revertValueContainer();
        }
      },

      /**
       * Value click handler.
       */
      handlerEdit: function( options ) {
        $._plumbereditable.disable( options );

        $.ajax({
          'method': 'GET',
          'async': true,
          'url': Drupal.settings.plumber.ajax.edit,
          'data': { 'path': options.path },
          'dataType': 'json',
          'success': function( data, textStatus ) {
            if ( $._plumbercommon.checkResponse( data ) && data.html ) {
              $._plumbereditable.attachForm( options, data );
            }
            else {
              $._plumbereditable.destroy( options );
            }
          },
          'error': function() {
            $._plumbereditable.destroy( options );
          }
        });
      }
    }
  });

  $.fn.extend({

    /**
     * Add or update the plumbertree behavior.
     */
    plumbertree: function() {
      this.each( function() {
        var tree = $( this );
        tree
          .find( '.plumber-tree-node:not(.plumber-node-processed)' )
          .addClass( 'plumber-node-processed' )
          .css( { cursor: "pointer" } )
          .plumbernode( { tree: tree } );
        tree
          .find( '.plumber-editable:not(.plumber-editable-processed)' )
          .addClass( 'plumber-editable-processed' )
          .css( { cursor: "pointer" } )
          .plumbereditable();
      });
    },

    /**
     * Add the plumbernode behavior on the given element.
     */
    plumbereditable: function( options ) {
      options = options || {};

      this.each(function() {
        var element = $( this );

        var localOptions = $.extend( {}, options, {
          valueContainer: element.find( '.plumber-column-value' ),
          path: element.attr( 'id' ),
        });

        $._plumbereditable.enable( localOptions );
      });

      return this;
    },

    /**
     * Add the plumbernode behavior on the given element.
     */
    plumbernode: function( options ) {
      options = options || {};

      this.each(function() {
        var element = $( this );

        if ( element.is( '.plumber-expandable' ) ) {

          var localOptions = $.extend( {}, options, {
            element: element,
            path: element.attr( 'id' ),
            isAjax: element.is( '.plumber-ajax' ),
            expanded: element.is( '.plumber-expanded' ),
          });
  
          localOptions.element.get(0)._plumbernode = localOptions;
  
          $._plumbernode.enable( localOptions );
        }

        // No AJAX or business here, only the ability to mark some columns.
        element.click(function( event ) {
          if ( element.is( '.plumber-tree-mark' ) )
            element.removeClass( 'plumber-tree-mark' );
          else
            element.addClass( 'plumber-tree-mark' );
        });
      });

      return this;
    }
  });

})( jQuery );
